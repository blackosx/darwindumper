# DarwinDumper

App to dump OS X system information to aid troubleshooting.

A collection of scripts and tools to provide a convenient method to quickly gather a system overview of your OS X System. The app can be run from either the Finder or command line, with the resulting dumps saved to a 'DarwinDumperReports' folder in the same directory of the main app.

The following can be dumped:

	- ACPI tables.
	- Audio codec and further info.
	- Boot loaders and configuration files.
	- CPU Info.
	- Device-properties.
	- Disk partition structure / info.
	- Disk sectors (hex).
	- DMI (SMBIOS).
	- EDID.
	- I/O Kit Registry.
	- EFI Memory Map.
	- EFI vars (some).
	- Kernel information
	- Kexts - (list of currently loaded kexts).
	- Memory.
	- NVRAM (both Apple specific and UEFI firmware variables).
	- SMC Keys.
	- LSPCI (PCI vendor & device codes) dumps.
	- Power (Sleep/Hibernate) settings.
	- RTC.
	- System Profiler.
	- System BIOS.
	- System Log(s).
	- Video BIOS.
	

The app can create an HTML report showing a complete overview. 

There is a 'privacy' option that will mask sensitive informationin the dumped files and the HTML report. For example, the IOPlatformSerialNumber, IOPlatformUUID, IOMACAddress(s) ,USB Serial Number(s), SystemSerialNumber, serial-number, fmm-mobileme-token-FMM, MLB and ROM efi vars.

Please see the [Wiki](https://bitbucket.org/blackosx/darwindumper/wiki/Home) for further information.

#License
[GPL v3](http://opensource.org/licenses/GPL-3.0)
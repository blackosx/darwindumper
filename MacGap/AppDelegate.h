//
//  AppDelegate.h
//  MG
//
//  Created by Tim Debo on 5/19/14.
//
//

#import <Cocoa/Cocoa.h>

@class WindowController;

@interface AppDelegate : NSObject <NSApplicationDelegate,NSUserNotificationCenterDelegate>

@property (retain, nonatomic) WindowController *windowController;

// Blackosx added
// ref: http://stackoverflow.com/questions/15842226/how-to-enable-main-menu-item-copy
@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSWindow *SparkleWindow;
@property (strong) IBOutlet NSMenuItem *openLog;
@property (strong) IBOutlet NSMenuItem *openSparklePref;
@property (strong) IBOutlet NSMenuItem *sparkleCheckForUpdates;


@property (assign) IBOutlet NSButton *SparkleAutoCheck;
@property (assign) IBOutlet NSButton *SparkleAutoDownload;
@property (assign) IBOutlet NSPopUpButton *SparkleTimingPopup;
@property (assign) IBOutlet NSTextField *SparkleLastUpdateField;

@end
